let latitude = "";
let longitude = "";
let picNum = 0;
let pageNum = 1;
let photoArray = [];

const findLocation = function () {

     
    function success(position) {
      latitude  = position.coords.latitude;
      longitude = position.coords.longitude;
      //console.log(`Latitude: ${latitude} °, Longitude: ${longitude} °`);
      getPhotos();
    }
  
    function error() {
        //36.169830, -115.155095
        latitude = 36.169830 //19.084793;
        longitude = -115.155095 //72.875685;
        //console.log(`Latitude: ${latitude} °, Longitude: ${longitude} °`);
        getPhotos();
    }
  
    if (!navigator.geolocation) {
        latitude = 19.084793;
        longitude = 72.875685;
        getPhotos();
    } else {
      
      navigator.geolocation.getCurrentPosition(success, error);
    }
  
  }
const displayPhotos = function (picNum) {
    //console.log(photoArray);
    if (picNum === undefined) picNum = 0;
    //if (picNum >= data.photos.photo.length) picNum = 0;
    if (photoArray !== undefined) {
        const displayArea = document.getElementById("displayArea");
        displayArea.innerHTML = "";
        const newImage = document.createElement("img");
        
        const newHeader = document.createElement('h2');
        const newAnchor = document.createElement('a');
        const anchorUrl = `https://www.flickr.com/photos/${photoArray[picNum].owner}/${photoArray[picNum].id}`
        const anchorText = document.createTextNode(`${photoArray[picNum].title}`);
        newAnchor.appendChild(anchorText);
        newAnchor.href = anchorUrl;
        newAnchor.title = "Flickr Picture"
        newHeader.appendChild(newAnchor);
        
        const imgUrl = `https://farm${photoArray[picNum].farm}.staticflickr.com/${photoArray[picNum].server}/${photoArray[picNum].id}_${photoArray[picNum].secret}.jpg`;
        newImage.src = imgUrl;
        
        
        //console.log(imgUrl);
        
        displayArea.appendChild(newImage);
        displayArea.appendChild(newHeader);
    }
    
   
}

const getPhotos = function (picNum) {
    photoArray = [];
    //console.log(`Latitude: ${latitude} °, Longitude: ${longitude} °`);
    //console.log("https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=7a060dcebe58acece371ba247c3b7079&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=" + latitude + "&lon=" + longitude + "&text=buildings");
    fetch("https://shrouded-mountain-15003.herokuapp.com/https://flickr.com/services/rest/?api_key=7a060dcebe58acece371ba247c3b7079&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&page=" + pageNum + "&lat=" + latitude + "&lon=" + longitude + "&text=buildings")
    .then(res => res.json())
    .then(data => {
        for (let index =0; index < data.photos.photo.length; index++){
            photoArray.push(data.photos.photo[index])
        }
        displayPhotos(picNum)});
    
}
const photoAdv = function(method) {
    if (method === "next" && picNum <= 3) {

        picNum ++;
        displayPhotos(picNum);
    }else if (method === "next" && picNum === 4) {
        pageNum++;
        picNum = 0;
        getPhotos(picNum);
    }else if(method === "prev" && picNum > 0) {
        picNum --;
        displayPhotos(picNum);
    }else {
        if (pageNum > 1) pageNum--;
        picNum = 4;
        getPhotos(picNum)
    }
    
}


  findLocation();
  